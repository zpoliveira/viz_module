import { Jogador } from './../../model/jogador';
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { EditarJogadorDto } from 'src/app/dto/editarJogadorDto';
import { EditarJogadorService } from './editar-jogador.service';

describe('EditarJogadorService', () => {
  let service: EditarJogadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EditarJogadorService]
    });
    service = TestBed.inject(EditarJogadorService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('updateUtilizador', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.inject(HttpTestingController);
      const editarJogadorDtoStub: EditarJogadorDto = {
        nome: "José Oliveira",
        email: "1161335@isep.ipp.pt",
        password: "12345678",
        telefone: "930000000",
        urlFacebook: "www.face.pt",
        urlLinkedIn: "www.link.pt",
        dataNascimento: "01/01/1985",
        humor: "NEUTRO",
        nacionalidade: "Portuguesa",
        cidadeResidencia: "Porto",
        descricaoBreve: "Descrição Breve",
        avatar: null,
        tagList: []
      };


      service.updateUtilizador(editarJogadorDtoStub).subscribe(res => {
        expect(res).toEqual(editarJogadorDtoStub);
      });
      const req = httpTestingController.expectOne(
        ' https://localhost:5001/api/Jogador/1161335@isep.ipp.pt'
      );
      expect(req.request.method).toEqual('PUT');
      req.flush(editarJogadorDtoStub);
      httpTestingController.verify();
    });
  });
});
