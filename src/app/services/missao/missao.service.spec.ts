import { TestBed } from '@angular/core/testing';

import { MissaoService } from './missao.service';

describe('MissaoService', () => {
  let service: MissaoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MissaoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
