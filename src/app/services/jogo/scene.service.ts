import { ElementRef, Injectable, NgZone, OnDestroy } from '@angular/core';
import { JogadoJogoDto } from 'src/app/dto/jogador/jogadorJogoDto';
import Caminho from "../../../assets/MockData/caminho.json";
import {
  AmbientLight,
  Box3,
  BoxGeometry,
  CanvasTexture,
  ClampToEdgeWrapping,
  Color,
  CylinderGeometry,
  Group,
  Intersection,
  Light,
  LinearFilter,
  Mesh,
  MeshBasicMaterial,
  MeshPhongMaterial,
  PerspectiveCamera,
  PlaneGeometry,
  PointLight,
  Raycaster,
  Scene,
  SpotLight,
  Sprite,
  SpriteMaterial,
  TextureLoader,
  Vector2,
  Vector3,
  WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import Dados from "../../../assets/MockData/ligacoes.json";
import { Observable, Subject } from 'rxjs';
import { ApiService } from '../shared/api.service';
import { isEmptyExpression } from '@angular/compiler';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class SceneService implements OnDestroy {
  private canvas!: HTMLCanvasElement;
  private renderer!: WebGLRenderer;
  private camera;
  private minimapCamera;
  private scene!: Scene;
  private light!: Light[];
  private orbitControls!: OrbitControls;
  private hoverContainer!: Group;

  public cubes: Group = new Group();
  private arrows: Group = new Group();
  private sprites: Group = new Group();
  public playerInfo = new Subject();
  sceneMeshes: Box3[] = []
  dir = new Vector3()
  intersects: Intersection[] = []

  player = new JogadoJogoDto();
  meuSet = new Set();
  meuMap = new Map();
  mapCaminhos = new Map();
  mouse = new Vector2();
  data_points = [];
  generated_points = [];
  point;
  container;
  raycaster;
  cameraBox;

  private frameId: number | null = null;

  color_array = [
    "#1f78b4",
    "#b2df8a",
    "#33a02c",
    "#fb9a99",
    "#e31a1c",
    "#fdbf6f",
    "#ff7f00",
    "#6a3d9a",
    "#cab2d6",
    "#ffff99"
  ]

  red_gradient = [
    "#FFEBEE",
    "#FFCDD2",
    "#E57373",
    "#E57373",
    "#EF5350",
    "#F44336",
    "#E53935",
    "#D32F2F",
    "#C62828",
    "#B71C1C"
  ]

  blue_gradient = [
    "#E3F2FD",
    "#BBDEFB",
    "#90CAF9",
    "#64B5F6",
    "#42A5F5",
    "#2196F3",
    "#1E88E5",
    "#1976D2",
    "#1565C0",
    "#0D47A1"
  ]

  public constructor(private ngZone: NgZone, private http: HttpClient) {
  }

  public ngOnDestroy(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }


  requestAnimationFrame(callback) {
    const requestAnimationFrame = window.requestAnimationFrame;
    requestAnimationFrame(callback);
  }

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    // The first step is to get the reference of the canvas element from our HTML document
    this.criarMapaCaminhos();
    this.player = new JogadoJogoDto();
    this.canvas = canvas.nativeElement;

    this.renderer = new WebGLRenderer({
      canvas: this.canvas,
      alpha: true,    // transparent background
      antialias: true // smooth edges
    });
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.renderer.setSize(width, height);
    this.renderer.setScissorTest(true);
    // this.renderer.shadowMap.enabled = true;
    this.container = document.getElementById("Threejs");
    this.container.appendChild(this.renderer.domElement);



    // create the scene
    this.scene = new Scene();
    this.scene.background = new Color(0x4C524F);

    this.hoverContainer = new Group();
    this.scene.add(this.hoverContainer);

    let aspectRatio = window.innerWidth / window.innerHeight;

    this.camera = new PerspectiveCamera(
      75, aspectRatio, 0.11, 1000
    );
    this.camera.position.fromArray([1, 10, 15]);
    this.camera.name = "Camara Principal";

    this.cameraBox = new Box3(new Vector3(), new Vector3);
    this.cameraBox.setFromObject(this.camera);


    //Foco de luz a acompanhar a camara
    var pointLight = new PointLight(0xffffff, 1, 100);
    pointLight.position.copy(this.camera.position);
    pointLight.castShadow = true;
    this.camera.add(pointLight);



    // Mini Map camera
    this.minimapCamera = new PerspectiveCamera(
      45, aspectRatio, 0.1, 1000
    );
    this.minimapCamera.lookAt(0, -10, 0);
    // this.minimapCamera = new OrthographicCamera((width/40) / - 2, (width/40) / 2, (height/40) / 2, (height/40) / - 2, 1, 1000 );
    this.minimapCamera.position.fromArray([0, 20, 0]);
    this.camera.add(this.minimapCamera)

    this.scene.add(this.camera);
    this.scene.add(this.minimapCamera);

    // soft white light
    this.createLight();

    const raycaster = new Raycaster();

    document.addEventListener("mousemove", (event) => onMouseMove(event, this.camera, this.cubes, this.renderer, this.data_points, raycaster, this));


    function onMouseMove(event, camera, cubes, renderer, points, raycaster, that) {
      event.preventDefault();
      //x
      let mouse3D;
      let rect = renderer.domElement.getBoundingClientRect();
      if (event.clientX && event.clientY) {
        const mouseX = (event.clientX - rect.left) / rect.width * 2.0 - 1.0;
        const mouseY = -(event.clientY - rect.top) / rect.height * 2.0 + 1.0;
        mouse3D = new Vector2(
          mouseX, mouseY
        );
      }

      raycaster.setFromCamera(mouse3D, camera);

      const intersects = raycaster.intersectObjects(cubes.children, false);
      if (intersects.length > 0) {
        let pos = intersects[0].object.parent.children.indexOf(intersects[0].object);
        highlightPoint(intersects[0], cubes);
        that.setTooltipPlayerData(points[pos]);
      } else {
        removeHighlights(cubes);
        that.removeTooltipPlayerData();
      }
    }

    document.addEventListener("mousedown", (event) => onMouseClick(event, this.camera, this.cubes, this.meuMap, this.arrows, this.renderer, this.data_points));
    function onMouseClick(event, camera, cubes, mapa, arrows, renderer, points) {
      event.preventDefault();
      //x
      let mouse3D;
      let rect = renderer.domElement.getBoundingClientRect();
      if (event.clientX && event.clientY) {
        const mouseX = (event.clientX - rect.left) / rect.width * 2.0 - 1.0;
        const mouseY = -(event.clientY - rect.top) / rect.height * 2.0 + 1.0;
        mouse3D = new Vector2(
          mouseX, mouseY
        );
      }

      raycaster.setFromCamera(mouse3D, camera);

      const intersects = raycaster.intersectObjects(cubes.children, false);
      if (intersects.length > 0) {
        let pos = intersects[0].object.parent.children.indexOf(intersects[0].object);
        var cubo_seleccionado = new String(points[pos].email).trim();
        point_selected(cubo_seleccionado, intersects[0], cubes, arrows, points, "0xF40A38", mapa);
      }
    }



    // Create controls
    this.createControls();
    this.createControlsMinimap();

    // process data
    this.initRede();

    this.createFloor();


    this.orbitControls.addEventListener('change', (event) => onChange(event, this.orbitControls, this.camera, this.sceneMeshes, this.cameraBox, this.renderer, this.scene));

    function onChange(event, orbitControls, camera, sceneMeshes, cameraBox, renderer, scene) {

      cameraBox.setFromObject(camera);
      let actualPosition: Vector3 = camera.position.clone()
      let previousPosition: Vector3;
      for (let mesh of sceneMeshes) {
        if (!mesh.containsPoint(actualPosition)) {
          previousPosition = actualPosition.clone();
          orbitControls.saveState();
        } else {
          camera.position.set(previousPosition.x * 1.05, previousPosition.y * 1.05, previousPosition.z * 1.05);
          renderer.render(scene, camera);
        }
      }
    }
    this.scene.add(this.cubes);
    this.scene.add(this.arrows);
    this.scene.add(this.sprites);
  }

  private createFloor(): void {
    // Floor
    var floorGeometry = new PlaneGeometry(window.innerWidth, window.innerHeight, 20, 20);
    var floorMaterial = new MeshPhongMaterial({
      color: 0x07A259,
      specular: 0x000000,
      shininess: 100
    });

    var floor = new Mesh(floorGeometry, floorMaterial);
    floor.rotation.x = -0.5 * Math.PI;
    floor.receiveShadow = true;
    this.scene.add(floor);
    floor.geometry.computeBoundingBox();
    var floorBox = new Box3();
    floorBox.setFromObject(floor);
    floorBox.expandByScalar(1.2);
    this.sceneMeshes.push(floorBox);
  }

  private createControls(): void {
    const controls = new OrbitControls(this.camera, this.renderer.domElement);
    controls.maxPolarAngle = Math.PI / 2.2;
    this.orbitControls = controls;

    document.addEventListener("keydown", (event) => onKeyDown(event, this.camera, this.player, this.orbitControls, this.cubes, this.arrows, this.generated_points, this.meuMap, this.cameraBox, this.sceneMeshes, this.renderer, this.scene));
    function onKeyDown(event, camera, player, controls, cubes, arrows, pontos, meuMap, cameraBox, sceneMeshes, renderer, scene) {
      event.preventDefault();
      var keyCode = event.which;

      if (keyCode == 87) {// w
        camera.position.z -= Math.cos(camera.rotation.y) * player.speed;
        camera.position.x -= Math.sin(camera.rotation.y) * player.speed;
        camera.position.y += Math.sin(camera.rotation.x) * player.speed;

        controls.target.z -= Math.cos(camera.rotation.y) * player.speed;
        controls.target.x -= Math.sin(camera.rotation.y) * player.speed;
        controls.target.y += Math.sin(camera.rotation.x) * player.speed;
      } else if (keyCode == 83) {// s
        camera.position.z += Math.cos(camera.rotation.y) * player.speed;
        camera.position.x += Math.sin(camera.rotation.y) * player.speed;
        camera.position.y -= Math.sin(camera.rotation.x) * player.speed;

        controls.target.z += Math.cos(camera.rotation.y) * player.speed;
        controls.target.x += Math.sin(camera.rotation.y) * player.speed;
        controls.target.y -= Math.sin(camera.rotation.x) * player.speed;
      } else if (keyCode == 65) {// a
        camera.position.z += Math.sin(camera.rotation.y) * player.speed;
        camera.position.x -= Math.cos(camera.rotation.y) * player.speed;

        controls.target.z += Math.sin(camera.rotation.y) * player.speed;
        controls.target.x -= Math.cos(camera.rotation.y) * player.speed;
      } else if (keyCode == 68) {// d
        camera.position.z -= Math.sin(camera.rotation.y) * player.speed;
        camera.position.x += Math.cos(camera.rotation.y) * player.speed;

        controls.target.z -= Math.sin(camera.rotation.y) * player.speed;
        controls.target.x += Math.cos(camera.rotation.y) * player.speed;
      } else if (keyCode == 37) {// la
        camera.rotation.y += player.turnSpeed;
      } else if (keyCode == 39) {// ra
        camera.rotation.y -= player.turnSpeed;
      } else if (keyCode == 38) {// ua
        camera.rotation.x += player.turnSpeed;
      } else if (keyCode == 40) {// da
        camera.rotation.x -= player.turnSpeed;
      }

      camera.rotation.order = 'YXZ';

      cameraBox.setFromObject(camera);
      let actualPosition: Vector3 = camera.position.clone()
      let previousPosition: Vector3;
      for (let mesh of sceneMeshes) {
        if (!mesh.containsPoint(actualPosition)) {
          previousPosition = actualPosition.clone();
        } else {
          camera.position.set(previousPosition.x * 1.05, previousPosition.y * 1.05, previousPosition.z * 1.05);
          renderer.render(scene, camera);
        }
      }
    }
  }

  private createControlsMinimap(): void {
    // const controls = new OrbitControls(this.minimapCamera, this.renderer.domElement);
    // controls.maxPolarAngle = Math.PI/2.2;
    // this.orbitControls = controls;
  }

  private createLight(): void {
    const ambientLight = new AmbientLight(0x090909);
    ambientLight.intensity = 3;

    var spotLight = new SpotLight(0xAAAAAA);
    spotLight.position.set(15, 3, 8);
    spotLight.angle = Math.PI / 4;
    spotLight.castShadow = true;
    spotLight.shadow.bias = 0.0001;
    spotLight.shadow.mapSize.width = 2048; // Shadow Quality
    spotLight.shadow.mapSize.height = 2048; // Shadow Quality

    var spotLight2 = new SpotLight(0xAAAAAA);
    spotLight2.position.set(-10, 3, 8);
    spotLight2.angle = Math.PI / 4;
    spotLight2.castShadow = true;
    spotLight2.shadow.bias = 0.0001;
    spotLight2.shadow.mapSize.width = 2048; // Shadow Quality
    spotLight2.shadow.mapSize.height = 2048; // Shadow Quality

    this.light = [ambientLight, spotLight, spotLight2];
    this.scene.add(...this.light);
  }

  public animate(): void {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }

      window.addEventListener('resize', () => {
        this.resize();
      });
    });
  }

  public render(): void {
    this.cameraBox.setFromObject(this.camera);
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    this.renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
    this.renderer.clear();

    //Main
    let left = Math.floor(window.innerWidth * 0);
    let bottom = Math.floor(window.innerHeight * 0);
    let width = Math.floor(window.innerWidth * 1.0);
    let height = Math.floor(window.innerHeight * 1.0);

    this.renderer.setViewport(left, bottom, width, height);

    this.renderer.setScissor(left, bottom, width, height);

    this.renderer.render(this.scene, this.camera);

    //Minimap
    left = Math.floor(window.innerWidth * 0.05);
    bottom = Math.floor(window.innerHeight * 0.05);
    width = Math.floor(window.innerWidth * 0.2);
    height = Math.floor(window.innerHeight * 0.2);

    this.renderer.setViewport(left, bottom, width, height);

    this.renderer.setScissor(left, bottom, width, height);

    this.renderer.render(this.scene, this.minimapCamera);

    this.renderer.setViewport(10, window.innerHeight - 160 - 10, 240, 160);
  }

  public resize(): void {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);

  }


  private async initRede() {

    let result;
    
    // (await this.getDados()).subscribe(r => {
    //   result = r
    //   console.log('RES: ', result)
    //   if (result.id === "ok") {
    //     "Traz JSON do BE"
    //   }
    //   return false
    // });

    this.traverse(Dados, this.process, 0);

    // TODO -> instanciar variáveis para imprimir
    let radius = 4;
    let size = this.meuSet.size;
    let rad;
    let points = [];
    let x, y, z;
    let playersByLevel = new Array(size).fill(0);

    // TODO -> criar um array para saber quantos jogadores há em cada nível, p. ex [1,2,3,4] tem 1 jogador no nível 0, 2 no nível 1, 3 no nível 2...
    for (let player of this.meuMap) {
      playersByLevel[player[1]] += 1;
    }

    // TODO -> limpa níveis vazios
    for (let i = playersByLevel.length; i > 0; i--) {
      if (playersByLevel[i] == 0)
        playersByLevel.splice(i, 1);
    }

    // TODO -> Copia o array de jogadores por nível para o usar como contador (por cada jogador imprimido, reduz-se 1 ao número de jogadores desse nivel até chegar a 0)
    let contadorPorNivel = [...playersByLevel];


    // Random point in circle
    function randomPosition(player, radius) {
      // TODO -> Método para criar array com as posições x,y e z de cada jogador, para depois se passar para o tree.js
      if (player[1] === 0) {
        x = 0;
        z = 0;
        points.push([x, player[0].listLigacaoLength, z]);
      } else {
        rad = (Math.PI * 2) / (playersByLevel[player[1]] / contadorPorNivel[player[1]]);
        contadorPorNivel[player[1]] -= 1;

        x = Math.cos(rad) * (radius * player[1]) + 0;
        z = Math.sin(rad) * (radius * player[1]) + 0;

        points.push([x, player[0].listLigacaoLength, z]);
      }

      return [x, player[0].listLigacaoLength, z];
    }



    for (let player of this.meuMap) {
      let position = randomPosition(player, radius);
      let name = 'Nome: ' + player[0].nome;
      let email = 'Email: ' + player[0].email;
      let humor = 'Humor: ' + player[0].humor;
      let avatar = 'Avatar: ' + player[0].avatar;
      if (player[0].avatar === 'null') {
        avatar = 'Avatar: ' + "../../../assets/images/user-default.png"
      }
      let group = player[1];
      let size = player[0].tags.length === 0 ? 0.5 : player[0].tags.length;
      let fromParent = player[0].fromParentLigacao;
      let toParent = player[0].toParentLigacao
      let point = { position, name, email, humor, avatar, size, group, fromParent, toParent };

      this.data_points.push(point);
    }

    let generated_points = this.data_points;

    for (let datum of generated_points) {
      // Set vector coordinates from data
      let size = 1 + ((datum.size * 10) / 100);
      var geometry = new BoxGeometry(size, size, size);
      var pic = datum.avatar.split(':')[1].trim();

      if (pic === 'null') {
        pic = "../../../assets/images/user-default.png"
      }

      var material = new MeshBasicMaterial({ // Required For Shadows
        map: new TextureLoader().load(pic)
      });

      var cube = new Mesh(geometry, material);
      cube.position.x = datum.position[0];
      cube.position.z = datum.position[2];
      cube.position.y = datum.position[1] + size / 2;
      cube.castShadow = true;
      cube.receiveShadow = true;

      this.cubes.add(cube);
      cube.geometry.computeBoundingBox();
      var cubeBox = new Box3();
      cubeBox.setFromObject(cube);
      cubeBox.expandByScalar(2);
      this.sceneMeshes.push(cubeBox);


      // Adicionar Billboard com humor
      const texture = new CanvasTexture(this.canvas);
      // because our canvas is likely not a power of 2
      // in both dimensions set the filtering appropriately.
      texture.minFilter = LinearFilter;
      texture.wrapS = ClampToEdgeWrapping;
      texture.wrapT = ClampToEdgeWrapping;

      let map: any;

      switch (datum.humor.split(':')[1].trim()) {
        case 'FELIZ':
          map = new TextureLoader().load('../../../assets/images/sun.png');
          break;
        case 'TRISTE':
          map = new TextureLoader().load('../../../assets/images/rain.png');
          break;
        case 'ZANGADO':
          map = new TextureLoader().load('../../../assets/images/lightning.png');
          break;
        case 'NEUTRO':
          map = new TextureLoader().load('../../../assets/images/fog.png');
          break;
        default:
          map = new TextureLoader().load('../../../assets/images/sun.png');
          break;
      }

      const labelMaterial = new SpriteMaterial({
        map: map,
        color: 0xffffff
      });

      const label = new Sprite(labelMaterial);
      label.position.x = datum.position[0] + (size * 1.2);
      label.position.y = datum.position[1] + (size * 1.2);
      label.position.z = datum.position[2];

      // if units are meters then 0.01 here makes size
      // of the label into centimeters.
      const labelBaseScale = 0.001;
      label.scale.x = this.canvas.width * (labelBaseScale * 0.8);
      label.scale.y = this.canvas.height * labelBaseScale;

      this.sprites.add(label);

    }


    for (let i = 0; i < this.data_points.length; i++) {
      let edgeGeometry;
      let edgeMaterial;
      let edgeGeometry2;
      let edgeMaterial2;
      if (this.data_points[i].group !== 0) {
        let size = 1 + ((this.data_points[i].size * 10) / 100);
        if (this.data_points[i].group === 1) {

          //const length = radius;
          const length = (Math.sqrt(Math.pow((this.data_points[i].position[0] - this.data_points[0].position[0]), 2) +
            Math.pow(((this.data_points[i].position[1] + size / 2) - (this.data_points[0].position[1] + size / 2)), 2) +
            Math.pow((this.data_points[i].position[2] - this.data_points[0].position[2]), 2)));

          let forcaFromParent = this.data_points[i].fromParent;
          if (forcaFromParent === 100) {
            forcaFromParent = 99;
          }
          let forcaToParent = this.data_points[i].toParent;
          if (forcaToParent === 100) {
            forcaToParent = 99;
          }
          const hex = this.red_gradient[Math.trunc(forcaFromParent / 10)];
          const hex2 = this.blue_gradient[Math.trunc(forcaToParent / 10)];

          // Create a cylinder geometry
          edgeGeometry = new CylinderGeometry(0.01, 0.15, 0.7);

          // Create a red-colored material
          edgeMaterial = new MeshBasicMaterial({ color: hex });

          // Create a cylinder geometry
          edgeGeometry2 = new CylinderGeometry(0.15, 0.01, 0.7);

          // Create a blue-colored material
          edgeMaterial2 = new MeshBasicMaterial({ color: hex2 });

          const cylinder = new Mesh(edgeGeometry, edgeMaterial);
          const cylinder2 = new Mesh(edgeGeometry2, edgeMaterial2);


          // Set its position
          cylinder.position.set((0 + this.data_points[i].position[0]) / 2.0, ((this.data_points[i].position[1] + (size / 2)) + (this.data_points[0].position[1] + size / 2)) / 2.0, (0 + this.data_points[i].position[2]) / 2.0);
          cylinder2.position.set((0 + this.data_points[i].position[0]) / 2.0, ((this.data_points[i].position[1] + (size / 2)) + (this.data_points[0].position[1] + size / 2)) / 2.0, (0 + this.data_points[i].position[2]) / 2.0);

          // Set its orientation
          const angH = Math.atan2(this.data_points[i].position[0] - 0, this.data_points[i].position[2] - 0);
          const angV = Math.acos(((this.data_points[i].position[1] + (size / 2)) - (this.data_points[0].position[1] + size / 2)) / length);

          cylinder.rotateY(angH);
          cylinder.rotateX(angV);
          cylinder2.rotateY(angH);
          cylinder2.rotateX(angV);

          // Set its length
          cylinder.scale.set(1.0, length, 1.0);
          cylinder2.scale.set(1.0, length, 1.0);

          // Add it to the scene
          this.arrows.add(cylinder)
          this.arrows.add(cylinder2)
          // this.sceneMeshes.push(cylinder);
          // this.sceneMeshes.push(cylinder2);
        } else {
          let playerPosition = 0;
          for (let player of this.meuMap) {
            let email = this.data_points[i].email.split(':')[1].trim();
            if (player[0].email == email) {
              break;
            }
            playerPosition++;
          }

          let parentPosition = 0;
          let parent;
          let level;
          for (let player of this.meuMap) {
            if (player[1] === this.data_points[i].group - 1) {
              parent = player;
              level = player[1] + 1;

              break;
            }
          }
          let array = [...this.meuMap.values()];
          for (let i = playerPosition; i > 0; i--) {
            if (array[i] === level - 1) {
              parentPosition = i;
              break;
            }
          }


          const length2 = (Math.sqrt(Math.pow((this.data_points[playerPosition].position[0] - this.data_points[parentPosition].position[0]), 2) +
            Math.pow(((this.data_points[playerPosition].position[1] + size / 2) - (this.data_points[parentPosition].position[1] + size / 2)), 2) +
            Math.pow((this.data_points[playerPosition].position[2] - this.data_points[parentPosition].position[2]), 2)));


          let forcaFromParent = this.data_points[playerPosition].fromParent;
          if (forcaFromParent === 100) {
            forcaFromParent = 99;
          }
          let forcaToParent = this.data_points[playerPosition].toParent;
          if (forcaToParent === 100) {
            forcaToParent = 99;
          }
          const hex = this.red_gradient[Math.trunc(forcaFromParent / 10)];
          const hex2 = this.blue_gradient[Math.trunc(forcaToParent / 10)];

          let edgeGeometry3 = new CylinderGeometry(0.01, 0.15, 1);

          // Create a red-colored material
          let edgeMaterial3 = new MeshBasicMaterial({ color: hex2 });
          const cylinder3 = new Mesh(edgeGeometry3, edgeMaterial3);

          let edgeGeometry4 = new CylinderGeometry(0.15, 0.01, 1);

          // Create a red-colored material
          let edgeMaterial4 = new MeshBasicMaterial({ color: hex });
          const cylinder4 = new Mesh(edgeGeometry4, edgeMaterial4);

          // Set its position
          cylinder3.position.set((this.data_points[parentPosition].position[0] + this.data_points[playerPosition].position[0]) / 2.0, ((this.data_points[playerPosition].position[1] + (size / 2)) + (this.data_points[parentPosition].position[1] + (size / 2))) / 2.0, (this.data_points[playerPosition].position[2] + this.data_points[parentPosition].position[2]) / 2.0);
          cylinder4.position.set((this.data_points[parentPosition].position[0] + this.data_points[playerPosition].position[0]) / 2.0, ((this.data_points[playerPosition].position[1] + (size / 2)) + (this.data_points[parentPosition].position[1] + size / 2)) / 2.0, (this.data_points[playerPosition].position[2] + this.data_points[parentPosition].position[2]) / 2.0);

          // Set its orientation
          const angH2 = Math.atan2(this.data_points[parentPosition].position[0] - this.data_points[playerPosition].position[0], this.data_points[parentPosition].position[2] - this.data_points[playerPosition].position[2]);
          const angV2 = Math.acos(((this.data_points[parentPosition].position[1] + (size / 2)) - (this.data_points[playerPosition].position[1] + (size / 2))) / length2);
          cylinder3.rotateY(angH2);
          cylinder3.rotateX(angV2);
          cylinder4.rotateY(angH2);
          cylinder4.rotateX(angV2);

          // Set its length
          cylinder3.scale.set(1.0, length2, 1.0);
          cylinder4.scale.set(1.0, length2, 1.0);

          this.arrows.add(cylinder3)
          this.arrows.add(cylinder4)
          // this.sceneMeshes.push(cylinder3);
          // this.sceneMeshes.push(cylinder4);


        }

      }
    }
  }

  // Transforma JSON em MAP(Jogador, nível)
  private process(key, value, nivel) {

    // Cria instância de jogador
    switch (key) {
      case 'jogador':
        this.player.nome = value.nome;
        this.player.email = value.email;
        this.player.tags = value.tags;
        this.player.humor = value.humor;
        this.player.avatar = value.avatar;
        break;
      case 'fromParent':
        if (value === null) {
          this.player.fromParentLigacao = null;
          this.player.fromParentRelacao = null;
          this.player.fromParentTags = [];
        } else {
          this.player.fromParentLigacao = value.forcaLigacao;
          this.player.fromParentRelacao = value.forcaRelacao;
          this.player.fromParentTags = value.tags;
        }
        break;
      case 'toParent':
        if (value === null) {
          this.player.toParentLigacao = null;
          this.player.toParentRelacao = null;
          this.player.toParentTags = [];
        } else {
          this.player.toParentLigacao = value.forcaLigacao;
          this.player.toParentRelacao = value.forcaRelacao;
          this.player.toParentTags = value.tags;
        }
        break;
      case 'listLigacoes':
        this.player.listLigacaoLength = value.length;
        break;

      default:
        break;
    }

    // Quando chega à mudança de nível, guarda o this.player no map
    if (key === 'listLigacoes') {
      if (!this.meuSet.has(this.player.email)) {
        this.meuSet.add(this.player.email);
        this.meuMap.set(this.player, nivel);
      }

      // Cria novo this.player vazio
      this.player = new JogadoJogoDto();

    }

    //console.log('XXXXXXXXXXXXXXXXXXXXXX');
    // TODO -> no meuMap tens {jogador, nível}
    // console.log('MAP: ', this.meuMap);
    // console.log('SET: ', this.meuSet);
    // console.log('XXXXXXXXXXXXXXXXXXXXXX');
  }

  // Método recursivo para correr JSON recebido da API
  private traverse(o, func, nivel) {
    for (var i in o) {
      // Chama função process para "trabalhar os dados recebidos"
      func.apply(this, [i, o[i], nivel]);
      if (o[i] !== null && typeof (o[i]) == "object") {
        // Descer um nível
        if (i === 'listLigacoes') {
          nivel++;
        }
        this.traverse(o[i], func, nivel);
      }
    }
    nivel--;
  }

  setTooltipPlayerData(playerData) {
    this.playerInfo.next(playerData);
  }

  removeTooltipPlayerData() {
    this.playerInfo.next(null);
  }

  criarMapaCaminhos() {

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Methods': ['GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS'],
        'Access-Control-Allow-Headers': ['Origin', 'Content-Type', 'x-paginacao']
      })
    };

    // let uri = environment.urlAddress;
    let uri = 'https://localhost:5001/api';
    const url = uri + `/relacionamento/caminhos`;
    let res2;
    let c1 = [{
      "nome": "Miguel Pereira",
      "email": "1181846@isep.ipp.pt",
    },
    {
      "nome": "José Oliveira",
      "email": "1161335@isep.ipp.pt",

    },
    {
      "nome": "Miguel Silva",
      "email": "1080321@isep.ipp.pt",
    },
    {
      "nome": "Steven H. Ryles",
      "email": "StevenHRyles@jourrapide.com",
    }];

    let c2 = [{
      "nome": "Miguel Pereira",
      "email": "1181846@isep.ipp.pt",
    },
    {
      "nome": "Maureen J. Moyer",
      "email": "MaureenJMoyer@teleworm.us",

    },
    {
      "nome": "June S. Sanders",
      "email": "JuneSSanders@rhyta.com",
    },
    {
      "nome": "Solomon K. Martin",
      "email": "SolomonKMartin@armyspy.com",
    }];

    let c3 = [{
      "nome": "Miguel Pereira",
      "email": "1181846@isep.ipp.pt",
    },
    {
      "nome": "Jorge Mota",
      "email": "1131345@isep.ipp.pt",

    },
    {
      "nome": "Cláudia Monteiro",
      "email": "1040233@isep.ipp.pt",
    },
    {
      "nome": "Genaro J. Herrera",
      "email": "GenaroJHerrera@teleworm.us",
    }];

    let c4 = [{
      "nome": "Miguel Pereira",
      "email": "1181846@isep.ipp.pt",
    },
    {
      "nome": "Laura M. Gipson",
      "email": "LauraMGipson@dayrep.com",

    },
    {
      "nome": "Eugene T. Schulz",
      "email": "EugeneTSchulz@dayrep.com",
    },
    {
      "nome": "John K. Irvine",
      "email": "JohnKIrvine@armyspy.com",
    }];

    // Descomentar para consumir do BE Master Data Dados

    // this.http.get(url, httpOptions,).subscribe(r => {
    //   res2 = r;
    //   console.log('RES: ', res2);
    //   if (res2.id === "ok") {
    //     this.mapCaminhos.set("StevenHRyles@jourrapide.com", c1);
    //     this.mapCaminhos.set("SolomonKMartin@armyspy.com", c2);
    //     this.mapCaminhos.set("GenaroJHerrera@teleworm.us", c3);
    //     this.mapCaminhos.set("JohnKIrvine@armyspy.com", c4);
    //   }

    // }

    this.mapCaminhos.set("StevenHRyles@jourrapide.com", c1);
    this.mapCaminhos.set("SolomonKMartin@armyspy.com", c2);
    this.mapCaminhos.set("GenaroJHerrera@teleworm.us", c3);
    this.mapCaminhos.set("JohnKIrvine@armyspy.com", c4);

  }

  async getDados() {

    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        'Access-Control-Allow-Methods': ['GET', 'POST', 'PATCH', 'PUT', 'DELETE', 'OPTIONS'],
        'Access-Control-Allow-Headers': ['Origin', 'Content-Type', 'x-paginacao']
      })
    };

    // let uri = environment.urlAddress;
    let uri = 'https://localhost:5001/api';
    const url = uri + '/relacionamento/caminhos';
    let res3;
    //Descomentar para consumir do BE Master Data Dados

    return await this.http.get(url, httpOptions);
    
}

}

function highlightPoint(mat, cubes) {
  removeHighlights(cubes);
  mat.object['material'].color.setHex(0xF40A38);

}
function removeHighlights(cubes: Group) {
  cubes.children.forEach(cube => cube['material'].color.setHex(0xFCFCFC));
}

function createCaminho(JSONCaminho, cubes, arrows, pontos, cor, meuMap) {
  const generatedCaminho = [];

  var count = 1;
  var count2 = 0;
  for (const key of meuMap.keys()) {
    let posicao = count;
    let email = key.email;
    let forca = key.listLigacaoLength;
    var seta;
    let seta_ini;
    let seta_fi;

    if (forca > 0) {
      if (count2 <= 0) {
        seta_ini = ((posicao - 1) * 2);
        seta_fi = (((posicao - 1) * 2) + 1);
        seta = new String(seta_ini + ":" + seta_fi).trim();
      } else {
        seta_ini = ((posicao - 1) * 2) - 2;
        seta_fi = (((posicao - 1) * 2) + 1) - 2;
        seta = new String(seta_ini + ":" + seta_fi).trim();
      }
    } else {
      count2 = count2 + 1;
      seta_ini = (posicao - 2) * 2;
      seta_fi = ((posicao - 2) * 2) + 1;
      seta = new String(seta_ini + ":" + seta_fi).trim();
    }
    //console.log("Posicao:",posicao);
    //console.log("Count:",count2);
    //console.log("Cubo:", email);
    //console.log("Seta:", seta); 
    const cubos_caminho = { posicao, email, forca, seta_ini, seta_fi };
    generatedCaminho.push(cubos_caminho);
    count = count + 1;
  }
  var data = JSON.parse(JSON.stringify(JSONCaminho));
  let cubos = cubes.children;
  let setas = arrows.children;
  //console.log("Setas:",setas);
  //console.log("Cubos:", cubos);

  var count = 0;
  for (let i = 0; i < pontos.length; i++) {
    for (let k = 0; k < data.caminho.length; k++) {
      var me = new String(pontos[i].email).trim();
      var you = new String("Email: " + data.caminho[k].email).trim();
      var isEquel = JSON.stringify(me) === JSON.stringify(you);

      if (isEquel == true) {
        //cubos[generatedCaminho[0].posicao]['material'].color.setHex(cor);
        for (let w = 0; w < generatedCaminho.length; w++) {

          if (JSON.stringify(data.caminho[k].email).trim() === JSON.stringify(generatedCaminho[w].email).trim()) {
            //console.log("Posicao: "+generatedCaminho[w].posicao + "     Seta_INI"+ generatedCaminho[w].seta_ini + "         SETA_FI:" + generatedCaminho[w].seta_fi);
            //cubos[generatedCaminho[w].posicao]['material'].color.setHex(cor);
            setas[generatedCaminho[w].seta_ini]['material'].color.setHex(cor);
            setas[generatedCaminho[w].seta_fi]['material'].color.setHex(cor);
          }
        }
      }
    }
  }
}

function point_selected(ponto_selecionado, mat, cubes, arrows, pontos, color, meuMap) {
  //alert(ponto_selecionado);
  createCaminho(Caminho, cubes, arrows, pontos, "0xFFFF00", meuMap);
}





