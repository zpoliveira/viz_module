import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { PedidoLigacaoDto } from 'src/app/dto/pedidoLigacaoDto';
import { PesquisarJogadorService } from './pesquisar-jogador.service';

describe('PesquisarJogadorService', () => {
  let service: PesquisarJogadorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PesquisarJogadorService]
    });
    service = TestBed.inject(PesquisarJogadorService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  describe('pedidoLigacao', () => {
    it('makes expected calls', () => {
      const httpTestingController = TestBed.inject(HttpTestingController);
      const pedidoLigacaoDtoStub: PedidoLigacaoDto = {
        emailJogadorRequisitante: "1161335@isep.ipp.pt",
        emailJogadorObjectivo: "1080321@isep.ipp.pt",
        forcaLigacao: 50
      }
      service.pedidoLigacao(pedidoLigacaoDtoStub).subscribe(res => {
        expect(res).toEqual(pedidoLigacaoDtoStub);
      });
      const req = httpTestingController.expectOne(
        ' https://localhost:5001/api/Introducao/pedidoLigacao'
      );
      expect(req.request.method).toEqual('POST');
      req.flush(pedidoLigacaoDtoStub);
      httpTestingController.verify();
    });
  });
});
