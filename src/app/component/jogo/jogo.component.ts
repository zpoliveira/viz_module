import { importExpr } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, HostListener, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SceneService } from 'src/app/services/jogo/scene.service';

@Component({
  selector: 'app-jogo',
  templateUrl: './jogo.component.html',
  styleUrls: ['./jogo.component.scss'],
  providers: [SceneService]
})

export class JogoComponent implements OnInit {
  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas!: ElementRef<HTMLCanvasElement>;
  
  helpMenu: FormGroup;

  public constructor(private engServ: SceneService) {}

  initForm() {
    this.helpMenu = new FormGroup({
      help: new FormControl(false),
      statistics: new FormControl(false),
      mini_map: new FormControl(false),
      user_interface: new FormControl(false),
      multiple_views: new FormControl(false)
    })
  }



  ngOnInit() {
    this.engServ.createScene(this.rendererCanvas);
    this.engServ.animate();
    this.initForm();
  }


}
