import { Component, ElementRef, HostListener, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { SceneService } from 'src/app/services/jogo/scene.service';

interface Player {
    name: string,
    email: string,
    group: string,
    humor: string
};

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})

export class TooltipComponent implements OnInit {

    public playerDataExists = false;

    public name:string;
    public email:string;
    public group:string;
    public avatar:string;

    constructor(private sceneService: SceneService){
    }

    ngOnInit(): void {
        this.sceneService.playerInfo.subscribe( (player) => { 
            this.playerDataExists = (player) ? true : false;
            if(player != null){
                this.avatar = player['avatar'].split(':')[1].trim();
                this.name = player['name'];
                this.email = player['email'];
                this.group = player['group'];
            }
            
        });
    }

}