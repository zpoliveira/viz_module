import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { FormGroup, FormBuilder, FormControl, FormGroupDirective, Validators, NgForm } from '@angular/forms';
import { ErrorMatcher } from '../../error-matcher';

import { Humor } from '../../model/humor';
import { EditarHumorDto } from '../../dto/editarHumorDto';
import { EstadosHumor } from '../../model/humor';
import { EditarHumorService } from '../../services/editar-humor/editar-humor.service';
import { MessagesService } from '../../services/messages/messages.service';

@Component({
  selector: 'app-editar-humor',
  templateUrl: './editar-humor.component.html',
  styleUrls: ['./editar-humor.component.css']
})
export class EditarHumorComponent implements OnInit {

  dados: FormGroup | any;
  listaEstadosHumor!: Humor[]; //= EstadosHumor;
  matcher = new ErrorMatcher();
  formBuilder = new FormBuilder();

  constructor(private humorService: EditarHumorService, private messagesService: MessagesService,
    private location: Location) {

  }

  ngOnInit() {
    this.getListaEstadosHumor();
    this.dados = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      humor: new FormControl(null, Validators.required)
    });
  }

  getListaEstadosHumor() {
    this.humorService.getEstadosHumor()
      .subscribe(lista => this.listaEstadosHumor = lista);
  }


  async onFormSubmit(formDirective: FormGroupDirective) {
    if (this.dados.valid) {
      //console.log(this.dados.value);

      let novoHumor = (this.dados.value as EditarHumorDto);
      //console.log(novoHumor);

      (this.humorService.patchEstadoHumor(novoHumor))
        .subscribe();

      formDirective.resetForm();
      this.dados.reset();

    } else {
      //console.log("Erro");
      throw Error("Error");
    }


  }


  goBack(): void {
    this.location.back();
  }



}


