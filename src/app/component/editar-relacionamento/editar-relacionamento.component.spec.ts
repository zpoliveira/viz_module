import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarRelacionamentoComponent } from './editar-relacionamento.component';

describe('EditarRelacionamentoComponent', () => {
  let component: EditarRelacionamentoComponent;
  let fixture: ComponentFixture<EditarRelacionamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EditarRelacionamentoComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarRelacionamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
