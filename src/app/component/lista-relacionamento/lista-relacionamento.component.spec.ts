import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRelacionamentoComponent } from './lista-relacionamento.component';

describe('ListaRelacionamentoComponent', () => {
  let component: ListaRelacionamentoComponent;
  let fixture: ComponentFixture<ListaRelacionamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListaRelacionamentoComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRelacionamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
