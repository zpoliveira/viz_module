import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';


@Directive({
    selector: '[toolTip]',
  })
  export class TooltipDirective {
  
    constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

    @HostListener('document:mousemove', ['$event']) documentClickEvent(
        $event: MouseEvent
      ) {
        let xPosition = $event.clientX - 350;
        let yPosition = $event.clientY - 370;
        this.renderer.setStyle(
          this.elementRef.nativeElement,
          'top',
          `${yPosition}px`
        );
        this.renderer.setStyle(
          this.elementRef.nativeElement,
          'left',
          `${xPosition}px`
        );
      }
  
  }